package bd.CKATEPTb.ItemLock;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class ItemLockCMD extends CommandBase {

	@Override
	public String getName() {
		return "itemlock";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/itemlock soul";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		EntityPlayer player = null;
		if (!(sender instanceof EntityPlayer)) {
			sender.sendMessage(new TextComponentString("§4Эта команда для игроков."));
			return;
		}
		player = (EntityPlayer) sender;
		if (!isPlayerOpped(player)) {
			sender.sendMessage(new TextComponentString("§4Недостаточно прав."));
			return;
		}
		if (player.getHeldItemMainhand().isEmpty()) {
			sender.sendMessage(new TextComponentString("§4В руке пусто."));
			return;
		}

		NBTTagCompound displayTag = player.getHeldItemMainhand().getOrCreateSubCompound("display");
		NBTTagList loresList = displayTag.getTagList("Lore", Constants.NBT.TAG_STRING);

		boolean remove = false;

		for (int i = 0; i < loresList.tagCount(); i++) {
			if (loresList.getStringTagAt(i).toLowerCase().contains("привязан к душе")) {
				loresList.removeTag(i);
				sender.sendMessage(new TextComponentString("§2Убрал привязку к душе."));
				remove = true;
				displayTag.setTag("Lore", loresList);
				return;
			}
		}

		if (!remove) {
			if (args != null && args.length >= 0) {
				loresList.appendTag(new NBTTagString("§aПредмет привязан к душе " + args[0]));
				displayTag.setTag("Lore", loresList);
				sender.sendMessage(new TextComponentString("§2Привязал предмет к душе " + args[0]));
				return;
			} else {
				sender.sendMessage(new TextComponentString("§4Вы забыли указать душу.\n §aПример: /itemlock Викинга"));
				return;
			}
		}

	}

	public static boolean isPlayerOpped(EntityPlayer player) {
		return FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getOppedPlayers()
				.getEntry(player.getGameProfile()) != null;
	}
}
