package bd.CKATEPTb.ItemLock;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.event.entity.item.ItemTossEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.entity.player.PlayerDropsEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent;

@Mod.EventBusSubscriber(modid = "itemlock")
public class LockedItemEvent {

	public Map<ItemStack, EntityPlayer> backItems = new ConcurrentHashMap<ItemStack, EntityPlayer>();

	@SubscribeEvent
	public void onItemPickUp(EntityItemPickupEvent e) {
		if (e.getEntity() instanceof EntityPlayer) {
			ItemStack item = e.getItem().getItem();
			EntityPlayer player = (EntityPlayer) e.getEntity();
			if (canPickUp(item, player)) {
				System.out.println("Запретил поднять " + item.getDisplayName() + " для игрока " + player.getName());
				e.setCanceled(true);
				return;
			}
		}
	}

	@SubscribeEvent
	public void onItemDrop(ItemTossEvent e) {
		ItemStack item = e.getEntityItem().getItem();
		EntityPlayer player = e.getPlayer();
		if (canDrop(item, player)) {
			e.setCanceled(true);
			player.addItemStackToInventory(item);
			return;
		}
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onPlayerDeath(PlayerDropsEvent e) {
		if (e.getEntityLiving() instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) e.getEntityLiving();
			Boolean keepInventory = player.world.getGameRules().getBoolean("keepInventory");
			if (!e.getDrops().isEmpty() && !keepInventory) {
				ArrayList<ItemStack> items = new ArrayList<ItemStack>();
				for (int i = 0; i < e.getDrops().size(); i++) {
					if (e.getDrops().get(i) != null) {
						EntityItem item = e.getDrops().get(i);
						if (!canDrop(item.getItem(), player)) {
							items.add(item.getItem());
						} else backItems.put(item.getItem(), player);
					}
				}
				e.setCanceled(true);
				for(ItemStack item : items) {
					EntityItem spawn = new EntityItem(player.world, player.posX, player.posY, player.posZ, item);
					player.world.spawnEntity(spawn);
				}
			}
		}
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void onPlayerRespawn(PlayerRespawnEvent e) {
		EntityPlayer player = e.player;
		for (Entry<ItemStack, EntityPlayer> entry : backItems.entrySet()) {
			if (entry.getValue().equals(player)) {
				player.addItemStackToInventory(entry.getKey());
				backItems.remove(entry.getKey());
			}
		}
	}

	public boolean canDrop(ItemStack item, EntityPlayer player) {
		NBTTagCompound displayTag = item.getOrCreateSubCompound("display");
		NBTTagList loresList = displayTag.getTagList("Lore", Constants.NBT.TAG_STRING);
		boolean canDrop = true;
		for (int i = 0; i < loresList.tagCount(); i++) {
			if (loresList.getStringTagAt(i).toLowerCase().contains("привязан к душе")) {
				canDrop = false;
				break;
			}
		}
		return !canDrop;
	}

	public boolean canPickUp(ItemStack item, EntityPlayer player) {
		NBTTagCompound displayTag = item.getOrCreateSubCompound("display");
		NBTTagList loresList = displayTag.getTagList("Lore", Constants.NBT.TAG_STRING);
		boolean canPickUp = true;
		for (int i = 0; i < loresList.tagCount(); i++) {
			if (loresList.getStringTagAt(i).toLowerCase().contains("привязан к душе")) {
				canPickUp = false;
				if (!player.getEntityData().hasKey(EntityPlayer.PERSISTED_NBT_TAG))
					player.getEntityData().setTag(EntityPlayer.PERSISTED_NBT_TAG, new NBTTagCompound());
				NBTTagCompound playerTag = player.getEntityData().getCompoundTag(EntityPlayer.PERSISTED_NBT_TAG);
				if (playerTag.hasKey("soul") && loresList.getStringTagAt(i).toLowerCase()
						.contains(playerTag.getString("soul").toLowerCase())) {
					canPickUp = true;
					break;
				}
			}
		}
		return !canPickUp;
	}

}
