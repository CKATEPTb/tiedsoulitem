package bd.CKATEPTb.ItemLock;

import net.minecraft.command.CommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@Mod(modid = "itemlock")
public class ItemLock {

	@SidedProxy(clientSide = "bd.CKATEPTb.ItemLock.CommonProxy", serverSide = "bd.CKATEPTb.ItemLock.CommonProxy")
	public static CommonProxy proxy;

	@EventHandler
	public void preInit(FMLPreInitializationEvent e) {
		proxy.preInit(e);
		MinecraftForge.EVENT_BUS.register(new LockedItemEvent());
	}
	
	@SideOnly(Side.SERVER)
	@EventHandler
	public void preServerInit(FMLPreInitializationEvent e) {
		CommandHandler ch = (CommandHandler) FMLCommonHandler.instance().getMinecraftServerInstance().getCommandManager();
		ch.registerCommand(new ItemLockCMD());
		ch.registerCommand(new PlayerSoulCMD());
		System.out.println("Создал команды ItemLock и PlayerSoul.");
	}

	@EventHandler
	public void init(FMLInitializationEvent e) {
		proxy.init(e);
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent e) {
		proxy.postInit(e);
	}

}
