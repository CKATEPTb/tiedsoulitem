package bd.CKATEPTb.ItemLock;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class PlayerSoulCMD extends CommandBase {

	@Override
	public String getName() {
		return "playersoul";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/playersoul player soul";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		EntityPlayer player = null;
		if (args == null) {
			sender.sendMessage(new TextComponentString("§4Не указан игрок."));
			return;
		}
		if (args.length != 2) {
			sender.sendMessage(new TextComponentString("§4Не указана душа"));
			return;
		}

		if (FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList()
				.getPlayerByUsername(args[0]) != null)
			player = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList()
					.getPlayerByUsername(args[0]);
		if (player == null) {
			sender.sendMessage(new TextComponentString("§4Игрок не найден"));
			return;
		}

		if (!player.getEntityData().hasKey(EntityPlayer.PERSISTED_NBT_TAG))
			player.getEntityData().setTag(EntityPlayer.PERSISTED_NBT_TAG, new NBTTagCompound());
		NBTTagCompound displayTag = player.getEntityData().getCompoundTag(EntityPlayer.PERSISTED_NBT_TAG);
		if (displayTag.hasKey("soul")) {
			displayTag.removeTag("soul");
			sender.sendMessage(new TextComponentString("§2Забрал душу у игрока"));
		}
		displayTag.setString("soul", args[1]);
		sender.sendMessage(new TextComponentString("§2Вселил в игрока " + player.getName() + " душу " + args[1]));
	}
}
